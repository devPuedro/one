var Vehicle = function(){
    var exports = {};
    exports.prototype = {};
    exports.prototype.init = function(mph) {
        if (mph === undefined)
            this.mph = 5;
        else
            this.mph = mph;
    };
    exports.prototype.go = function() {
        console.log("Going " + this.mph.toString() + " mph.");
    };

    exports.create = function(mph) {
        var ret = Object.create(exports.prototype);
        ret.init(mph);
        return ret;
    };

    return exports;
};

module.exports = Vehicle;
