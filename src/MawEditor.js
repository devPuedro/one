import styles from './maweditor.css'
//import "font-awesome-webpack";
import TextArea from './TextArea.js';
export default {
    data () {
        return {
            textarea: null,
            boxResult: true
        }
    },
    computed: {},
    mounted () {
        this.textarea = new TextArea(document.getElementById("maw-content"));
    },
    methods: {
        bold () {
            this.textarea.bold();
        },
        italic () {
            this.textarea.italic();
        },
        strikethrough () {
            this.textarea.strikethrough();
        },
        openResult () {
            if(this.boxResult) {
                this.boxResult = false;
            } else {
                this.boxResult = true;
            }
        }
    },
    watch: {
        textarea: function(val, oldVal) {
            if(this.textarea!=null)
                this.textarea.render();
        }
    },
    components: {}
}
