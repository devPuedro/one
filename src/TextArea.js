import showdown from "showdown";
export default class TextArea {

    constructor(el) {
        this.$el = el;
    }

    getInputSelection () {
        var start = 0, end = 0, normalizedValue, range,
            textInputRange, len, endRange;

        if (typeof this.$el.selectionStart == "number" && typeof this.$el.selectionEnd == "number") {
            start = this.$el.selectionStart;
            end = this.$el.selectionEnd;
        } else {
            range = document.selection.createRange();

            if (range && range.parentElement() == this.$el) {
                len = this.$el.value.length;
                normalizedValue = this.$el.value.replace(/\r\n/g, "\n");

                // Create a working TextRange that lives only in the input
                textInputRange = this.$el.createTextRange();
                textInputRange.moveToBookmark(range.getBookmark());

                // Check if the start and end of the selection are at the very end
                // of the input, since moveStart/moveEnd doesn't return what we want
                // in those cases
                endRange = this.$el.createTextRange();
                endRange.collapse(false);

                if (textInputRange.compareEndPoints("StartToEnd", endRange) > -1) {
                    start = end = len;
                } else {
                    start = -textInputRange.moveStart("character", -len);
                    start += normalizedValue.slice(0, start).split("\n").length - 1;

                    if (textInputRange.compareEndPoints("EndToEnd", endRange) > -1) {
                        end = len;
                    } else {
                        end = -textInputRange.moveEnd("character", -len);
                        end += normalizedValue.slice(0, end).split("\n").length - 1;
                    }
                }
            }
        }

        return {
            start: start,
            end: end
        };
    }

    bold() {
        var sel = this.getInputSelection(this.$el), val = this.$el.value;
        var sel0 = this.$el.value.substring(this.$el.selectionStart, this.$el.selectionEnd);
        this.$el.value = val.slice(0, sel.start) + "**" + sel0 + "**" + val.slice(sel.end);
        this.render();
    }

    italic () {
        var sel = this.getInputSelection(this.$el), val = this.$el.value;
        var sel0 = this.$el.value.substring(this.$el.selectionStart, this.$el.selectionEnd);
        this.$el.value = val.slice(0, sel.start) + "*" + sel0 + "*" + val.slice(sel.end);
        this.render();
    }

    strikethrough () {
        var sel = this.getInputSelection(this.$el), val = this.$el.value;
        var sel0 = this.$el.value.substring(this.$el.selectionStart, this.$el.selectionEnd);
        this.$el.value = val.slice(0, sel.start) + "~~" + sel0 + "~~" + val.slice(sel.end);
        this.render();
    }

    surround (number) {

    }

    render() {
        var converter = new showdown.Converter();
        converter.setOption('strikethrough', true);
        converter.setOption('tables', true);
        converter.setOption('tablesHeaderId', true);
        this.$el.onkeyup = this.$el.onmouseup = () => {
            document.getElementById("maw-result").innerHTML = converter.makeHtml(this.$el.value);
        }
        /*var md = window.markdownit({
            html: true,        // Enable HTML tags in source
        });

        this.$el.onkeyup = this.$el.onmouseup = () => {
            var result = md.render(this.$el.value);
            document.getElementById("maw-result").innerHTML = result;
        }*/
    }
}
